package com.example.joel.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.joel.dto.LoginRequestDTO;
import com.example.joel.dto.SignUpRequestDTO;
import com.example.joel.exception.APIResponse;
import com.example.joel.model.JwtUtils;
import com.example.joel.service.LoginRegisterService;

@RestController
@RequestMapping(path = "api/v1/user")
public class LoginRegisterController {

	@Autowired
	private LoginRegisterService loginRegisterService;
	
	@Autowired
    private JwtUtils jwtUtils;
	
	@PostMapping("/signup")
	public ResponseEntity<APIResponse> signUp(@RequestBody SignUpRequestDTO signUpRequestDTO ){
	
	    APIResponse apiResponse = loginRegisterService.signUp(signUpRequestDTO);
	
	    return ResponseEntity
	            .status(apiResponse.getStatus())
	            .body(apiResponse);
	}
	
	@PostMapping("/login")
	public ResponseEntity<APIResponse> login(@RequestBody LoginRequestDTO loginRequestDTO ){
	
	    APIResponse apiResponse = loginRegisterService.login(loginRequestDTO);
	
	    return ResponseEntity
	            .status(apiResponse.getStatus())
	            .body(apiResponse);
	}
	
	@GetMapping("/privateApi")
    public ResponseEntity<APIResponse> privateApi(@RequestHeader(value = "authorization", defaultValue = "") String auth) throws Exception {
        APIResponse apiResponse =new APIResponse();

        jwtUtils.verify(auth);

        apiResponse.setData("this is private api");
        return ResponseEntity.status(apiResponse.getStatus()).body(apiResponse);
    }
}
