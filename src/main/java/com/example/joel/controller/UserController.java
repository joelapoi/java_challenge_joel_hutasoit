package com.example.joel.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.joel.exception.APIResponse;
import com.example.joel.model.Users;
import com.example.joel.service.UserService;


@RestController
@RequestMapping(path = "api/v1/user")
public class UserController {

	@Autowired
	public UserService userService;
	
	
	@GetMapping
	public List<Users> getUser() {
		return userService.getUser();
		
	}
	
	@PutMapping(path = "{Id}")
	private ResponseEntity<APIResponse> updateUser(@PathVariable("Id") Long Id, @RequestParam(required = false) String username, @RequestParam(required = false) String password) {
		 APIResponse apiResponse = userService.updateUser(Id, username, password);
			
		    return ResponseEntity
		            .status(apiResponse.getStatus())
		            .body(apiResponse);
		
	}
}
