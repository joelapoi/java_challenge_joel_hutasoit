package com.example.joel.serviceImpl;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.joel.exception.APIResponse;
import com.example.joel.exception.GlobalExceptionHandler;
import com.example.joel.model.Users;
import com.example.joel.repository.UserRepository;
import com.example.joel.service.UserService;


@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;


	@Override
	public List<Users> getUser() {
		return userRepository.findAll();
	}


	@Override
	public APIResponse updateUser(Long id, String username, String password) {
		APIResponse apiResponse = new APIResponse();
		Optional<Users> users = userRepository.findById(id);
		if (users == null) {
			apiResponse.setStatus(409);
			apiResponse.setData("Id" + id + "Tidak Tersedia");
			return apiResponse;
		}

		Users users1 = new Users();
		Users findUsername = userRepository.findByUsername(username);
		if (Objects.equals(findUsername, username)) {
			apiResponse.setData("Username sudah terpakai");
			apiResponse.setStatus(409);
			
			return apiResponse;
		}else if (Objects.equals(users1.getPassword(), password)) {
			apiResponse.setData("Password tidak boleh sama dengan password sebelumnya");
			apiResponse.setStatus(400);
			
			return apiResponse;
		}else if (username != null && username.length() > 0 && !Objects.equals(users1.getUsername(), username)) {
			users1.setId(id);
			users1.setUsername(username);
			users1.setPassword(password);
			users1 = userRepository.save(users1);
			
			apiResponse.setData(users1);
			apiResponse.setStatus(201);
			return apiResponse;
		}
		return apiResponse;
	}

	
	
}
