package com.example.joel.serviceImpl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.joel.dto.LoginRequestDTO;
import com.example.joel.dto.SignUpRequestDTO;
import com.example.joel.exception.APIResponse;
import com.example.joel.model.JwtUtils;
import com.example.joel.model.Users;
import com.example.joel.repository.UserRepository;
import com.example.joel.service.LoginRegisterService;

@Service
public class LoginRegisterServiceImpl implements LoginRegisterService {

	@Autowired
    private UserRepository userRepository;
	
    @Autowired
    private JwtUtils jwtUtils;

    public APIResponse signUp(SignUpRequestDTO signUpRequestDTO) {
        APIResponse apiResponse = new APIResponse();

        // validation

        // dto to entity
        Users userEntity = new Users();
        
        userEntity.setUsername(signUpRequestDTO.getUsername());
        userEntity.setPassword(signUpRequestDTO.getPassword());
        
        Users users = userRepository.findByUsername(signUpRequestDTO.getUsername());

        if (users == null) {
        	// store entity
            userEntity = userRepository.save(userEntity);
            // generate jwt
            String token = jwtUtils.generateJwt(userEntity);
            
            Map<String , Object> data = new HashMap<>();
            data.put("accessToken", token);

            apiResponse.setData(data);
            apiResponse.setStatus(201);

            // return
            return apiResponse;
		}else {
			apiResponse.setStatus(409);
			apiResponse.setData("Username sudah terpakai");
			return apiResponse;
		}
        

        
    }

    public APIResponse login(LoginRequestDTO loginRequestDTO) {

        APIResponse apiResponse = new APIResponse();

        // validation

        // verify user exist with given email and password
        Users users = userRepository.findOneByUsernameIgnoreCaseAndPassword(loginRequestDTO.getUsername(), loginRequestDTO.getPassword());

        // response
        if(users == null){
            apiResponse.setData("Username dan / password kosong");
            apiResponse.setStatus(400);
            return apiResponse;
        }

        // generate jwt
        String token = jwtUtils.generateJwt(users);

        Map<String , Object> data = new HashMap<>();
        data.put("accessToken", token);

        apiResponse.setData(data);

        return apiResponse;
    }
}
