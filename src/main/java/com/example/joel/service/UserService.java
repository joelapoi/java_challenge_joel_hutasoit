package com.example.joel.service;


import java.util.List;

import com.example.joel.exception.APIResponse;
import com.example.joel.model.Users;

public interface UserService {

	public List<Users> getUser();

	public APIResponse updateUser(Long id, String username, String password);
}
