package com.example.joel.service;


import com.example.joel.dto.LoginRequestDTO;
import com.example.joel.dto.SignUpRequestDTO;
import com.example.joel.exception.APIResponse;

public interface LoginRegisterService {
	public APIResponse signUp(SignUpRequestDTO signUpRequestDTO);
	
	public APIResponse login(LoginRequestDTO loginRequestDTO);
}
