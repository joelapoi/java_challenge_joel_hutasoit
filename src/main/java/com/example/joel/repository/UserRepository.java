package com.example.joel.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.joel.model.Users;

@Repository
public interface UserRepository extends JpaRepository<Users, Long>{

	Users findOneByUsernameIgnoreCaseAndPassword(String username, String password);
	
	Users findByUsername(String username);
	
	Users findByUsernameAndPassword(String username, String password);
}
